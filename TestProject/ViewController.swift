//
//  ViewController.swift
//  TestProject
//
//  Created by Никита on 8.04.21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var greenView: UIView!
    @IBOutlet var redView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(redView.convert(redView.bounds.origin, from: self.view))
        print(redView.convert(redView.bounds.origin, to: self.view))
        let test = UIView(frame: .init(x: 104, y: 291, width: 40, height: 40))
        test.backgroundColor = .yellow
        view.addSubview(test)
    }
}
